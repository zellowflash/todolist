<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Todo extends CI_Model{
    /*
     * Get todolist
     */

    function __construct() {
        parent::__construct();
        $username = "root";
        $password = "";
        $database = "todolist";
        $this->mysqli = new mysqli("localhost", $username, $password, $database);
    }

    function __destruct() {
        $this->mysqli->close();
    }

    function getRows($id = ""){
        if(!empty($id)){
            
            $query = "SELECT * FROM todolist WHERE `id` = $id;";

            return $this->mysqli->query("$query");
        }else{
            
            $query = "SELECT * FROM todolist; ";

            return $this->mysqli->query("$query");
        }
    }

    function getRowsOn($date){
        $query = "SELECT * FROM todolist WHERE  start_date <= '$date' AND end_date >= '$date'";

        return $this->mysqli->query("$query");
    }
    
    /*
     * Insert post
     */
    public function insert($data = array()) {
        $work_name = $data['work_name'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $status = $data['status'];
        $query="INSERT INTO `todolist` (`id`, `work_name`, `start_date`, `end_date`, `status`)
        VALUES (NULL, '$work_name', '$start_date', '$end_date', '$status');";
        return $this->mysqli->query("$query");
    }
    
    /*
     * Update post
     */
    public function update($data, $id) {
        $work_name = $data['work_name'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $status = $data['status'];

        $query = "UPDATE todolist SET work_name='$work_name',
         start_date='$start_date',
         end_date='$end_date',
         status='$status'
         WHERE id=$id";

        return $this->mysqli->query("$query");
    }
    
    /*
     * Delete post
     */
    public function delete($id){
        $query = "DELETE FROM `todolist` WHERE `id` = $id;";

        return $this->mysqli->query("$query");
    }
}