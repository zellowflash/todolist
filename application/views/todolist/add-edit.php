<div class="container">
    <div class="col-xs-12">
    <?php 
        if(!empty($success_msg)){
            echo '<div class="alert alert-success">'.$success_msg.'</div>';
        }elseif(!empty($error_msg)){
            echo '<div class="alert alert-danger">'.$error_msg.'</div>';
        }
    ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $action; ?> Todo <a href="<?php echo site_url('index.php/todolist/'); ?>" class="glyphicon glyphicon-arrow-left pull-right"></a></div>
                <div class="panel-body">
                    <form method="post" action="" class="form">
                        <div class="form-group">
                            <label for="title">Work name</label>
                            <input type="text" class="form-control" name="work_name" value="<?php echo !empty($post['work_name'])?$post['work_name']:''; ?>">
                            <?php echo form_error('title','<p class="help-block text-danger">','</p>'); ?>
                        </div>

                        <div class="form-group">
                            <label for="title">Start Date</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="start_date" class="form-control" id="datepicker1" value="<?php echo !empty($post['start_date'])?$post['start_date']:''; ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title">End Date</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="end_date" class="form-control" id="datepicker2"  value="<?php echo !empty($post['end_date'])?$post['end_date']:''; ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title">Status</label>
                            <input type="text" class="form-control" name="status" value="<?php echo !empty($post['status'])?$post['status']:''; ?>">
                            <?php echo form_error('title','<p class="help-block text-danger">','</p>'); ?>
                        </div>

                        <script type="text/javascript">
                            $(function () {
                                $('#datepicker1').datepicker({dateFormat: 'yy-mm-dd'});;
                                $('#datepicker2').datepicker({dateFormat: 'yy-mm-dd'});;
                            });
                        </script>

                        <input type="submit" name="postSubmit" class="btn btn-primary" value="Submit"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>