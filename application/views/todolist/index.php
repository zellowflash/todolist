<div class="container">
    <?php if(!empty($success_msg)){ ?>
    <div class="col-xs-12">
        <div class="alert alert-success"><?php echo $success_msg; ?></div>
    </div>
    <?php }elseif(!empty($error_msg)){ ?>
    <div class="col-xs-12">
        <div class="alert alert-danger"><?php echo $error_msg; ?></div>
    </div>
    <?php } ?>


    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="post" action="" class="form">

                        <div class="form-group">
                            <label for="title">Works on date</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="date-of-works" class="form-control" id="datepicker1"  value="<?php echo !empty($dateOfWorks)?$dateOfWorks:''; ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(function () {
                                $('#datepicker1').datepicker({dateFormat: 'yy-mm-dd'});;
                            });
                        </script>

                        <input type="submit" name="postSubmit" class="btn btn-primary" value="Submit"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default ">
                <div class="panel-heading">Todo List <a href="<?php echo site_url('index.php/todolist/add/'); ?>" class="glyphicon glyphicon-plus pull-right" ></a></div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="30%">Work name</th>
                            <th width="15%">Start Date</th>
                            <th width="15%">End Date</th>
                            <th width="15%">Status</th>
                        </tr>
                    </thead>
                    <tbody id="userData">
                        <?php if(!empty($todos)): foreach($todos as $todo): ?>
                        <tr>
                            <td><?php echo '#'.$todo['id']; ?></td>
                            <td><?php echo $todo['work_name']; ?></td>
                            <td><?php echo $todo['start_date']; ?></td>
                            <td><?php echo $todo['end_date']; ?></td>
                            <td><?php echo $todo['status']; ?></td>

                            <td>
                                <a href="<?php echo site_url('index.php/todolist/edit/'.$todo['id']); ?>" class="glyphicon glyphicon-edit"></a>
                                <a href="<?php echo site_url('index.php/todolist/delete/'.$todo['id']); ?>" class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure to delete?')"></a>
                            </td>
                        </tr>
                        <?php endforeach; else: ?>
                        <tr><td colspan="4">Post(s) not found......</td></tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>