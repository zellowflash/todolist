<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Todolist extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('todo');
    }
    
    public function index(){

        $dateOfWorks = $this->input->post('date-of-works');
        $data = array();
        
        //get messages from the session
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        if(empty($dateOfWorks))
            $data['todos'] = $this->todo->getRows();
        else
        {
            $data['todos'] = $this->todo->getRowsOn($dateOfWorks);
            $data['dateOfWorks'] = $dateOfWorks;
        }
        $data['title'] = 'To do list';
        
        //load the list page view
        $this->load->view('templates/header', $data);
        $this->load->view('todolist/index', $data);
        $this->load->view('templates/footer');
    }
    
    /*
     * Post details
     */
    public function view($id){
        $data = array();
        
        //check whether post id is not empty
        if(!empty($id)){
            $data['post'] = $this->todo->getRows($id);
            $data['title'] = $data['post']['title'];
            
            //load the details page view
            $this->load->view('templates/header', $data);
            $this->load->view('posts/view', $data);
            $this->load->view('templates/footer');
        }else{
            redirect('/posts');
        }
    }
    
    /*
     * Add post content
     */
    public function add(){
        $data = array();
        $todoData = array();
        
        //if add request is submitted
        if($this->input->post('postSubmit')){
            //form field validation rules
            $this->form_validation->set_rules('work_name', 'work name', 'required');
            $this->form_validation->set_rules('start_date', 'start date', 'required');
            $this->form_validation->set_rules('end_date', 'end date', 'required');
            $this->form_validation->set_rules('status', 'status', 'required');
            
            //prepare post data
            $todoData = array(
                'work_name' => $this->input->post('work_name'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
            );
            
            //validate submitted form data
            if($this->form_validation->run() == true){
                //insert post data
                $insert = $this->todo->insert($todoData);

                if($insert){
                    $this->session->set_userdata('success_msg', 'Post has been added successfully.');
                    redirect('/index.php/todolist');
                }else{
                    $data['error_msg'] = 'Some problems occurred, please try again.';
                }
            }
        }
        
        $data['post'] = $todoData;
        $data['title'] = 'Create Post';
        $data['action'] = 'Add';
        
        //load the add page view
        $this->load->view('templates/header', $data);
        $this->load->view('todolist/add-edit', $data);
        $this->load->view('templates/footer');
    }
    
    /*
     * Update post content
     */
    public function edit($id){
        $data = array();
        
        //get post data
        $todoData = $this->todo->getRows($id)->fetch_assoc();
        
        //if update request is submitted
        if($this->input->post('postSubmit')){
            //form field validation rules
            $this->form_validation->set_rules('work_name', 'work name', 'required');
            $this->form_validation->set_rules('start_date', 'start date', 'required');
            $this->form_validation->set_rules('end_date', 'end date', 'required');
            $this->form_validation->set_rules('status', 'status', 'required');
            
            //prepare cms page data
            $todoData = array(
                'work_name' => $this->input->post('work_name'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
            );
            
            //validate submitted form data
            if($this->form_validation->run() == true){
                //update post data
                $update = $this->todo->update($todoData, $id);

                if($update){
                    $this->session->set_userdata('success_msg', 'Post has been updated successfully.');
                    redirect('/index.php/todolist');
                }else{
                    $data['error_msg'] = 'Some problems occurred, please try again.';
                }
            }
        }

        
        $data['post'] = $todoData;
        $data['title'] = 'Update Todo';
        $data['action'] = 'Edit';
        
        //load the edit page view
        $this->load->view('templates/header', $data);
        $this->load->view('todolist/add-edit', $data);
        $this->load->view('templates/footer');
    }
    
    /*
     * Delete post data
     */
    public function delete($id){
        //check whether post id is not empty
        if($id){
            //delete post
            $delete = $this->todo->delete($id);
            
            if($delete){
                $this->session->set_userdata('success_msg', 'Post has been removed successfully.');
            }else{
                $this->session->set_userdata('error_msg', 'Some problems occurred, please try again.');
            }
        }

        redirect('index.php/todolist');
    }
}